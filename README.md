* Designed for use with automated CI pipelines.
* Currently for x86_64 only. Looking for help with other architectures.
* I'll sign my stuff when I get around to it :v

## Install latest version right now:
```bash
apk add crosstool-ng --no-cache --allow-untrusted --repository https://lexxyfox.gitlab.io/alpine-crosstool-ng
```

## OR install with updates:

```bash
echo https://lexxyfox.gitlab.io/alpine-crosstool-ng >> /etc/apk/repositories
apk add -U --allow-untrusted crosstool-ng
```

